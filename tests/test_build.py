"""
Test the build process and execute the resulting binary to ensure correct output
"""
# Import python libs
import os
import sys
import subprocess
import shutil

# Import pop libs
import pop.hub

# Import third party libs
import yaml

CWD = os.path.dirname(__file__)
PDIR = os.path.join(CWD, "pb")
CONF = os.path.join(PDIR, "pb.yml")

sys.path.insert(0, PDIR)


def test_build():
    os.chdir(PDIR)
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="pop_build")
    build_dir = os.path.join(PDIR, "build")
    sys.argv.append("--config")
    sys.argv.append(CONF)
    # Uncomment to run pkgbuild tests
    # sys.argv.append("--pkg-tgt")
    # sys.argv.append("arch")
    dist_dir = os.path.join(PDIR, "dist")
    pb_bin = os.path.join(dist_dir, "pb")
    hub.pop_build.init.cli()
    cp = subprocess.run(
        pb_bin, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    assert b"pb works!" in cp.stdout
    assert b"hashids works!" in cp.stdout
    shutil.rmtree(dist_dir)
    shutil.rmtree(build_dir)
